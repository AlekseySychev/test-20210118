<?php

namespace AlekseySychev;

require_once './vendor/autoload.php';

use AlekseySychev\System\DB;
use AlekseySychev\System\Router;

DB::connect('localhost', 'mysql', 'mysql', 'promo');

// показ рекламы
Router::get('/promo/', '\AlekseySychev\Controllers\PromoController@show');
// создание рекламы
Router::post('/promo/create/', '\AlekseySychev\Controllers\PromoController@create');
// обновление рекламы
Router::post('/promo/:id/update/', '\AlekseySychev\Controllers\PromoController@update');

Router::run();