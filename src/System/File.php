<?php

namespace AlekseySychev\System;

class File
{

    private static function getDir()
    {
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/banners';
        if (!is_dir($dir)) {
            mkdir($dir);
        }
        return $dir;
    }

    public static function save($file)
    {
        $name = md5($file['tmp_name'] . time()) . '-' . $file['name'];

        move_uploaded_file($file['tmp_name'], self::getDir() . '/' . $name);

        return $name;
    }

    public static function delete($name)
    {
        unlink(self::getDir() . '/' . $name);
    }

    public static function getUrl($name)
    {
        return '/banners/' . $name;
    }
}