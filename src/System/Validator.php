<?php

namespace AlekseySychev\System;

class Validator
{
    private $data = null;
    private $errors = null;

    private $fields = [
        'text' => 'текст объявления',
        'price' => 'стоимость одного показа',
        'amount' => 'лимит показов',
        'file' => 'картинка',
        'banner' => 'картинка',
    ];
    private $rules = [
        'required' => 'Поле `<name>` отсутствует в запросе',
        'minlen' => 'Поле `<name>` должно быть равно или длиннее `<param>`',
        'maxlen' => 'Поле `<name>` должно быть равно или короче `<param>`',
        'min' => 'Поле `<name>` должно быть равно или больше `<param>`',
        'max' => 'Поле `<name>` должно быть равно или меньше `<param>`',
    ];


    public function __construct($data, $fields)
    {
        foreach ($fields as $fieldName => $fieldRules) {
            if (is_string($fieldRules)) {
                $fieldRules = explode(',', $fieldRules);
            }
            foreach ($fieldRules as $fieldRule) {
                // парсим правило
                $ruleData = [];
                $fieldRule = explode(':', $fieldRule);
                // название правила всегда первым
                $ruleData[] = array_shift($fieldRule);
                // если что то осталось, то это параметры правила
                if (count($fieldRule)) {
                    $ruleData = array_merge($ruleData, $fieldRule);
                }
                // правило обязательного поля
                if ($ruleData[0] == 'required') {
                    if (!isset($data[$fieldName])) {
                        $this->errors[] = $this->getError($fieldName, $ruleData);
                    }
                }
                // правило минимальной длины строки
                if ($ruleData[0] == 'minlen') {
                    if (strlen($data[$fieldName]) <= $ruleData[1]) {
                        $this->errors[] = $this->getError($fieldName, $ruleData);
                    }
                }
                // правило максимальной длины строки
                if ($ruleData[0] == 'maxlen') {
                    if (strlen($data[$fieldName]) >= $ruleData[1]) {
                        $this->errors[] = $this->getError($fieldName, $ruleData);
                    }
                }
                // правило минимального значения
                if ($ruleData[0] == 'min') {
                    if ($data[$fieldName] <= $ruleData[1]) {
                        $this->errors[] = $this->getError($fieldName, $ruleData);
                    }
                }
                // правило максимального значения
                if ($ruleData[0] == 'max') {
                    if ($data[$fieldName] >= $ruleData[1]) {
                        $this->errors[] = $this->getError($fieldName, $ruleData);
                    }
                }
                // сохраняем данные
                $this->data[$fieldName] = isset($data[$fieldName]) ? $data[$fieldName] : null;
            }
        }
    }

    public function data()
    {
        return $this->data;
    }

    public function getError($fieldName, $ruleData)
    {
        $error = $this->rules[array_shift($ruleData)];
        $error = str_replace('<name>', $this->fields[$fieldName], $error);

        foreach ($ruleData as $param) {
            $pos = strpos($error, '<param>');
            if ($pos !== false) {
                $error = substr_replace($error, $param, $pos, strlen('<param>'));
            }
        }

        return $error;
    }

    public function errors()
    {
        return $this->errors;
    }
}