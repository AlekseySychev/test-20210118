<?php

namespace AlekseySychev\System;

class Router
{
    private static $routes = [];

    public static function post(String $route, String $callable)
    {
        self::$routes['post'][$route] = $callable;
    }

    public static function get(String $route, String $callable)
    {
        self::$routes['get'][$route] = $callable;
    }

    public static function put(String $route, String $callable)
    {
        self::$routes['put'][$route] = $callable;
    }

    public static function delete(String $route, String $callable)
    {
        self::$routes['delete'][$route] = $callable;
    }

    public static function run()
    {
        $variables = [];
        $currentRoute = trim($_GET['route'], '/');
        $currentRouteArray = explode('/', $currentRoute);
        $currentRouteLength = count($currentRouteArray);
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        foreach (self::$routes as $method => $routes) {
            // если текущий метод отличается от описываемого
            if ($requestMethod !== $method) continue;

            foreach ($routes as $route => $callable) {
                $route = trim($route, '/');
                $routeArray = explode('/', $route);

                // если текущий маршрут не равен по длине, пропускаем
                if (count($routeArray) != $currentRouteLength) continue;

                foreach ($currentRouteArray as $currentRouteArrayKey => $currentRouteArrayPiece) {
                    $routeArrayPiece = $routeArray[$currentRouteArrayKey];
                    // если текущий элемент не просто путь, а переменная, сохраняем её
                    // иначе проверяем на соответствие маршруту
                    if (strpos($routeArrayPiece, ':') !== false) {
                        $variables[str_replace(':', '', $routeArrayPiece)] = $currentRouteArrayPiece;
                    } elseif ($routeArrayPiece !== $currentRouteArrayPiece) {
                        break 2;
                    }
                }
                // вызываем нужную функцию передавая ей параметры на вход
                list($callableClass, $callableMethod) = explode('@', $callable);
                if (!class_exists($callableClass)) {
                    throw new \Exception("Ошибка маршрутизатора. Класс ($callableClass) не найден.");
                }
                if (!method_exists($callableClass, $callableMethod)) {
                    throw new \Exception("Ошибка маршрутизатора. Метод ($callableMethod) не найден.");
                }

                // переменные из запроса
                $request = self::getRequest();
                // вызов нужного метода
                $callableClass::$callableMethod(array_merge($variables, $request));
                return;
            }
        }
        throw new \Exception("Ошибка маршрутизатора. Маршрут ($currentRoute) не найден.");
    }

    private static function getRequest()
    {
        // параметры из гет и post запросов
        $variables = array_merge($_GET, $_POST, $_FILES);
        // убираем маршрут из параметров
        unset($variables['route']);
        // возвращаем переменные запроса
        return $variables;
    }
}