<?php

namespace AlekseySychev\System;

class DB
{
    private static $mysqli = null;

    public static function connect($host, $user, $password, $database)
    {
        self::$mysqli = new \mysqli($host, $user, $password, $database);

    }

    public static function escape($string)
    {
        if (is_string($string)) {
            return mysqli_real_escape_string(self::$mysqli, $string);
        } else {
            return $string;
        }

    }

    public static function rows($query)
    {
        $rows = [];
        $result = self::query($query);
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }

    public static function row($query)
    {
        $rows = self::rows($query);
        if (count($rows)) {
            return array_shift($rows);
        } else {
            return null;
        }
    }

    public static function query($query)
    {
        return mysqli_query(self::$mysqli, $query);
    }
}