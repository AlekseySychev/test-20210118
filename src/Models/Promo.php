<?php

namespace AlekseySychev\Models;

use AlekseySychev\System\DB;
use AlekseySychev\System\File;

class Promo
{
    private static $error = null;
    private static $fields = [
        'id',
        'text',
        'price',
        'showed',
        'limit',
        'banner'
    ];

    private static function getSetData($data)
    {
        // массив данных
        $set = [];
        foreach ($data as $name => $value) {
            if (in_array($name, self::$fields)) {
                $set[] = sprintf("`%s` = '%s'", DB::escape($name), DB::escape($value));
            }
        }
        return $set;
    }

    public static function show($promoId)
    {
        // сбрасываем ошибку
        self::setError();
        $query = sprintf("SELECT * FROM `promo` WHERE `showed` < `limit` ORDER BY `price` DESC LIMIT 1", $promoId);
        $promo = DB::row($query);
        if ($promo) {
            $promo['showed']++;
            $query = sprintf("UPDATE `promo` SET `showed` = '%s' WHERE `id` = '%s';", $promo['showed'], $promo['id']);
            DB::query($query);
            return $promo;
        } else {
            self::$error = 'Нет объявлений для показа';
        }
    }

    public static function find($id)
    {
        $query = sprintf("SELECT * FROM `promo` WHERE `id` = '%s';", $id);
        return DB::row($query);
    }

    public static function create($data)
    {
        // сбрасываем ошибку
        self::setError();
        // если есть данные, создаем
        if (count(self::getSetData($data))) {
            $query = sprintf("INSERT INTO `promo` SET %s;", implode(' ,', self::getSetData($data)));
            DB::query($query);
        } else {
            self::$error = 'Нет данных для создания';
        }
    }

    public static function update($data)
    {
        // сбрасываем ошибку
        self::setError();
        $id = $data['id'];

        $promo = self::find($id);
        // если есть такое поле
        if ($promo) {
            if ($promo['banner']) {
                File::delete($promo['banner']);
            }
            // если есть данные, создаем
            if (count(self::getSetData($data))) {
                $query = sprintf("UPDATE `promo` SET %s WHERE `id` = '%s';", implode(' ,', self::getSetData($data)), $id);
                DB::query($query);
            } else {
                self::$error = 'Нет данных для обновления';
            }
        } else {
            self::$error = 'Элемент не найден';
        }
    }

    public static function setError($error = null)
    {
        self::$error = $error;
    }

    public static function getError()
    {
        return self::$error;
    }
}