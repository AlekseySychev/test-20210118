<?php

namespace AlekseySychev\Controllers;

use AlekseySychev\Models\Promo;
use AlekseySychev\System\File;
use AlekseySychev\System\Responce;
use AlekseySychev\System\Validator;

class PromoController
{

    public function show($request)
    {
        $promo = Promo::show($request['id']);
        if (Promo::getError()) {
            Responce::json([
                'success' => false,
                'errors' => Promo::getError(),
            ], 204);
        } else {
            if ($promo['banner']) {
                $promo['banner'] = File::getUrl($promo['banner']);
            }
            Responce::json([
                'success' => true,
                'promo' => $promo,
            ]);
        }
    }

    public function create($request)
    {
        $validator = new Validator($request, [
            'text' => 'required,minlen:3,maxlen:120',
            'price' => 'required,min:0',
            'limit' => 'required,min:0',
            'banner' => '',
        ]);

        if ($validator->errors()) {
            Responce::json([
                'success' => false,
                'errors' => $validator->errors(),
            ], 400);
        } else {
            $data = $validator->data();
            if ($data['banner']) {
                $data['banner'] = File::save($data['banner']);
            }
            Promo::create($data);
            if (Promo::getError()) {
                Responce::json([
                    'success' => false,
                    'errors' => Promo::getError(),
                ], 400);
            } else {
                Responce::json([
                    'success' => true,
                ]);
            }
        }
    }

    public function update($request)
    {
        $validator = new Validator($request, [
            'id' => 'required',
            'text' => 'required,minlen:3,maxlen:120',
            'price' => 'required,min:0',
            'limit' => 'required,min:0',
            'banner' => '',
        ]);

        if ($validator->errors()) {
            Responce::json([
                'success' => false,
                'errors' => $validator->errors(),
            ], 400);
        } else {
            $data = $validator->data();
            if ($data['banner']) {
                $data['banner'] = File::save($data['banner']);
            }
            Promo::update($data);
            if (Promo::getError()) {
                Responce::json([
                    'success' => false,
                    'errors' => Promo::getError(),
                ], 400);
            } else {
                Responce::json([
                    'success' => true,
                ]);
            }
        }
    }
}